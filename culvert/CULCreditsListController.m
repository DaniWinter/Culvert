#include "CULCreditsListController.h"

@implementation CULCreditsListController

+ (NSString *)hb_shareText {
	return [NSString stringWithFormat:@"I love using #Culvert on my jailbroken %@ for a fresh wallpaper every day!", [UIDevice currentDevice].localizedModel];
}

+ (NSURL *)hb_shareURL {
	return [NSURL URLWithString:@"https://repo.d4ni.nl/"];
}

- (instancetype)init {
  UIColor *tweakColor = [UIColor colorWithRed:0.00 green:0.00 blue:0.00 alpha:1.0];

  self = [super init];

  if (self) {
    HBAppearanceSettings *appearanceSettings = [[HBAppearanceSettings alloc] init];
    appearanceSettings.tableViewCellTextColor = tweakColor;
    appearanceSettings.tintColor = tweakColor;
		appearanceSettings.invertedNavigationBar = YES;
    self.hb_appearanceSettings = appearanceSettings;
  }

  return self;
}

- (NSArray *)specifiers {
	if (!_specifiers) {
		_specifiers = [[self loadSpecifiersFromPlistName:@"Credits" target:self] retain];
	}

	return _specifiers;
}

@end
