#import <Preferences/Preferences.h>
#import <CepheiPrefs/HBRootListController.h>
#import <CepheiPrefs/HBSupportController.h>

@interface CULCreditsListController : HBRootListController
+ (NSString *)hb_shareText;
+ (NSURL *)hb_shareURL;
@end

@interface HBAppearanceSettings : NSObject
@property (readwrite, copy, nonatomic, nullable) UIColor *tableViewCellTextColor;
@property (readwrite, copy, nonatomic, nullable) UIColor *tintColor;
@property (assign, readwrite, nonatomic) BOOL invertedNavigationBar;
@end

@interface HBRespringController : NSObject
+ (void)respring;
@end
