#include "CULRootListController.h"

static BOOL             writeToLogFile = YES;

static NSFileHandle    *logHandle;
static NSFileManager   *logManager;
static NSString *const  logPath        = @"/Library/Application Support/Culvert/log.log";
static NSDateFormatter *dateFormatter  = nil;
static NSString *prefBundle = @"/var/mobile/Library/Preferences/nl.d4ni.culvert.plist";
static NSString *paypalEmail = @"";


@implementation CULRootListController

+ (NSString *)hb_shareText {
	return [NSString stringWithFormat:@"I love using #Culvert on my jailbroken %@ for a fresh wallpaper every day!", [UIDevice currentDevice].localizedModel];
}

+ (NSURL *)hb_shareURL {
	return [NSURL URLWithString:@"https://repo.d4ni.nl/"];
}

//Initiate header image
- (void) viewDidLoad {
	[super viewDidLoad];

	paypalEmail = [[[NSMutableDictionary alloc] initWithContentsOfFile:prefBundle] objectForKey:@"paypalEmail"];

	FSLog(@"[Culvert]: View did load.");

	if (![[NSFileManager defaultManager] fileExistsAtPath:@"/Library/Application Support/Culvert/wowdudeyoupaidtoremoveadsthanks"]) { //initiate ads if this file doesn't exist.
		self.bannerView = [[GADBannerView alloc]
	      initWithAdSize:kGADAdSizeSmartBannerPortrait];

	  [self addBannerViewToView:self.bannerView];

		self.bannerView.adUnitID = @"ca-app-pub-7530386759960560/9686663815";
	  self.bannerView.rootViewController = self;
	  [self.bannerView loadRequest:[GADRequest request]];

		self.bannerView.delegate = self;
	}

	float headerWidth = [[UIScreen mainScreen] bounds].size.width;
	float headerHeight = (headerWidth/100)*46.48985959438378; // woah, exact value and stuff.

	CGRect frame = CGRectMake(0, 0, headerWidth, headerHeight);

	UIImage *headerImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle bundleWithPath:@"/Library/PreferenceBundles/Culvert.bundle"] pathForResource:@"banner" ofType:@"jpg"]];

	UIImageView *headerView = [[UIImageView alloc] initWithFrame:frame];
	[headerView setImage:headerImage];
	headerView.backgroundColor = [UIColor blackColor];
	[headerView setContentMode:UIViewContentModeScaleAspectFit];
	[headerView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];

	self.table.tableHeaderView = headerView;
}

/// Tells the delegate an ad request loaded an ad.
- (void)adViewDidReceiveAd:(GADBannerView *)adView {
  FSLog(@"adViewDidReceiveAd");
	[self addBannerViewToView:self.bannerView];
}

/// Tells the delegate an ad request failed.
- (void)adView:(GADBannerView *)adView
    didFailToReceiveAdWithError:(GADRequestError *)error {
  FSLog(@"adView:didFailToReceiveAdWithError: %@", [error localizedDescription]);
}

/// Tells the delegate that a full-screen view will be presented in response
/// to the user clicking on an ad.
- (void)adViewWillPresentScreen:(GADBannerView *)adView {
  FSLog(@"adViewWillPresentScreen");
}

/// Tells the delegate that the full-screen view will be dismissed.
- (void)adViewWillDismissScreen:(GADBannerView *)adView {
  FSLog(@"adViewWillDismissScreen");
}

/// Tells the delegate that the full-screen view has been dismissed.
- (void)adViewDidDismissScreen:(GADBannerView *)adView {
  FSLog(@"adViewDidDismissScreen");
}

/// Tells the delegate that a user click will open another app (such as
/// the App Store), backgrounding the current app.
- (void)adViewWillLeaveApplication:(GADBannerView *)adView {
  FSLog(@"adViewWillLeaveApplication");
}

- (void)addBannerViewToView:(UIView *)bannerView {
  bannerView.translatesAutoresizingMaskIntoConstraints = NO;
  [self.view addSubview:bannerView];
  [self.view addConstraints:@[
    [NSLayoutConstraint constraintWithItem:bannerView
                               attribute:NSLayoutAttributeBottom
                               relatedBy:NSLayoutRelationEqual
                                  toItem:self.bottomLayoutGuide
                               attribute:NSLayoutAttributeTop
                              multiplier:1
                                constant:0],
    [NSLayoutConstraint constraintWithItem:bannerView
                               attribute:NSLayoutAttributeCenterX
                               relatedBy:NSLayoutRelationEqual
                                  toItem:self.view
                               attribute:NSLayoutAttributeCenterX
                              multiplier:1
                                constant:0]
                                ]];
}

- (instancetype)init {
  UIColor *tweakColor = [UIColor colorWithRed:0.00 green:0.00 blue:0.00 alpha:1.0];

  self = [super init];

  if (self) {
    HBAppearanceSettings *appearanceSettings = [[HBAppearanceSettings alloc] init];
    appearanceSettings.tableViewCellTextColor = tweakColor;
    appearanceSettings.tintColor = tweakColor;
		appearanceSettings.invertedNavigationBar = YES;
    self.hb_appearanceSettings = appearanceSettings;
  }

  return self;
}

- (NSArray *)specifiers {
	if (!_specifiers) {
		_specifiers = [[self loadSpecifiersFromPlistName:@"Root" target:self] retain];
	}

	return _specifiers;
}

-(id) readPreferenceValue:(PSSpecifier *)specifier {
		NSDictionary *plistFile = [NSDictionary dictionaryWithContentsOfFile:[NSString stringWithFormat:@"/User/Library/Preferences/%@.plist", [specifier.properties objectForKey:@"defaults"]]];
		if (![plistFile objectForKey:[specifier.properties objectForKey:@"key"]]) {
			return [specifier.properties objectForKey:@"default"];
		}
		return [plistFile objectForKey:[specifier.properties objectForKey:@"key"]];
}

-(void) setPreferenceValue:(id)value specifier:(PSSpecifier *)specifier {
	NSMutableDictionary *plistFile = [[NSMutableDictionary alloc] initWithContentsOfFile:[NSString stringWithFormat:@"/User/Library/Preferences/%@.plist", [specifier.properties objectForKey:@"defaults"]]];
	[plistFile setObject:value forKey:[specifier.properties objectForKey:@"key"]];
	[plistFile writeToFile:[NSString stringWithFormat:@"/User/Library/Preferences/%@.plist", [specifier.properties objectForKey:@"defaults"]] atomically:1];
	if ([specifier.properties objectForKey:@"PostNotification"]) {
		CFNotificationCenterPostNotification(CFNotificationCenterGetDarwinNotifyCenter(), (CFStringRef)[specifier.properties objectForKey:@"PostNotification"], NULL, NULL, YES);
	}
	[super setPreferenceValue:value specifier:specifier];
}

- (void)updateWallpaper {
	CFNotificationCenterPostNotification(CFNotificationCenterGetDarwinNotifyCenter(), CFSTR("nl.d4ni.culvert/update"), nil, nil, true);
}

-(void)support {
  UIViewController *viewController = (UIViewController *)[HBSupportController supportViewControllerForBundle:[NSBundle bundleForClass:self.class] preferencesIdentifier:@"nl.d4ni.culvert"];
	[self.navigationController pushViewController:viewController animated:YES];
}

-(void)save {
	[HBRespringController respring];
}

static void FSLog(NSString *format, ...) {
    va_list args;
    va_start(args, format);
    NSString *formattedMessage = [[NSString alloc] initWithFormat:format arguments:args];
    va_end(args);
    // FSLog(@"%@: %@", @"[Culvert]", formattedMessage);

    if ( writeToLogFile ) {
        if ( !dateFormatter)
            dateFormatter = [[NSDateFormatter alloc] init];

        [dateFormatter setDateFormat:@"M/d/yy, h:mm:ss aa"];

        if ( !logManager )
            logManager = [NSFileManager defaultManager];

        if ( ![logManager fileExistsAtPath:logPath] ) {
            [[NSString stringWithFormat: @"%@ - %@\n", [[dateFormatter stringFromDate:[NSDate date]] lowercaseString], formattedMessage] writeToFile:logPath atomically:YES encoding: NSUTF8StringEncoding error:NULL];
        } else {
            logHandle = [NSFileHandle fileHandleForWritingAtPath:logPath];
            [logHandle seekToEndOfFile];
            [logHandle writeData:[[NSString stringWithFormat: @"%@ - %@\n", [[dateFormatter stringFromDate:[NSDate date]] lowercaseString], formattedMessage] dataUsingEncoding:NSUTF8StringEncoding]];
        }
    }

    [formattedMessage release];
}
// End Logging


@end
