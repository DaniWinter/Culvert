#include "CULPaymentListController.h"

static NSString *prefBundle = @"/var/mobile/Library/Preferences/nl.d4ni.culvert.plist";
static NSString *paypalEmail = @"";
static NSString *licensePath = @"/Library/Application Support/Culvert/wowdudeyoupaidtoremoveadsthanks";
static NSFileManager *licenseManager;

@implementation CULPaymentListController

- (instancetype)init {
  UIColor *tweakColor = [UIColor colorWithRed:0.00 green:0.00 blue:0.00 alpha:1.0];

  self = [super init];

  if (self) {
    HBAppearanceSettings *appearanceSettings = [[HBAppearanceSettings alloc] init];
    appearanceSettings.tableViewCellTextColor = tweakColor;
    appearanceSettings.tintColor = tweakColor;
		appearanceSettings.invertedNavigationBar = YES;
    self.hb_appearanceSettings = appearanceSettings;
  }

  return self;
}

- (NSArray *)specifiers {
	if (!_specifiers) {
		_specifiers = [[self loadSpecifiersFromPlistName:@"Payment" target:self] retain];
	}

	return _specifiers;
}

-(void)retrieveLicense {
	[self.view endEditing:YES];
	licenseManager = [NSFileManager defaultManager];

	UIAlertController *alertController1 = [UIAlertController alertControllerWithTitle:@"Culvert" message:@"\nChecking license, please wait a few seconds.." preferredStyle:UIAlertControllerStyleAlert];
	[[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertController1 animated:YES completion:NULL];

	double delayInSeconds = 5.0;
	dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
	dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
		[alertController1 dismissViewControllerAnimated:YES completion:nil];

		paypalEmail = [[[NSMutableDictionary alloc] initWithContentsOfFile:prefBundle] objectForKey:@"paypalEmail"];

		NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://api.d4ni.nl/culvert/license.php?package=nl.d4ni.culvert&paypal=%@", paypalEmail]]];
		NSData *theData = [NSURLConnection sendSynchronousRequest:request
												returningResponse:nil
																		error:nil];
		NSDictionary *newJSON = [NSJSONSerialization JSONObjectWithData:theData
																													options:0
																														error:nil];
		if ([@"ok" isEqualToString:[newJSON objectForKey:@"status"]]) {
			if (![licenseManager fileExistsAtPath:licensePath]) {
				NSData *fileContents = [@"THANKS!" dataUsingEncoding:NSUTF8StringEncoding];
				[licenseManager createFileAtPath:licensePath
				                                contents:fileContents
	                                attributes:nil];
			}

			UIAlertController *alertController1 = [UIAlertController alertControllerWithTitle:@"Culvert" message:@"\nYour license has been activated!\nThe settings app will now close." preferredStyle:UIAlertControllerStyleAlert];
			[alertController1 addAction:[UIAlertAction actionWithTitle:@"OK." style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
	      exit(0);
			}]];
			[[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertController1 animated:YES completion:NULL];
		} else {
			if ([licenseManager fileExistsAtPath:licensePath]) {
				NSError *error = nil;
				[licenseManager removeItemAtPath:licensePath error:&error];
			}
			UIAlertController *alertController1 = [UIAlertController alertControllerWithTitle:@"Culvert" message:@"\nCouldn't find your license..\nPlease try again." preferredStyle:UIAlertControllerStyleAlert];
			[alertController1 addAction:[UIAlertAction actionWithTitle:@"OK." style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}]];
			[[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertController1 animated:YES completion:NULL];
		}
	});
}

-(id) readPreferenceValue:(PSSpecifier *)specifier {
		NSDictionary *plistFile = [NSDictionary dictionaryWithContentsOfFile:[NSString stringWithFormat:@"/User/Library/Preferences/%@.plist", [specifier.properties objectForKey:@"defaults"]]];
		if (![plistFile objectForKey:[specifier.properties objectForKey:@"key"]]) {
			return [specifier.properties objectForKey:@"default"];
		}
		return [plistFile objectForKey:[specifier.properties objectForKey:@"key"]];
}

-(void) setPreferenceValue:(id)value specifier:(PSSpecifier *)specifier {
	NSMutableDictionary *plistFile = [[NSMutableDictionary alloc] initWithContentsOfFile:[NSString stringWithFormat:@"/User/Library/Preferences/%@.plist", [specifier.properties objectForKey:@"defaults"]]];
	[plistFile setObject:value forKey:[specifier.properties objectForKey:@"key"]];
	[plistFile writeToFile:[NSString stringWithFormat:@"/User/Library/Preferences/%@.plist", [specifier.properties objectForKey:@"defaults"]] atomically:1];
	if ([specifier.properties objectForKey:@"PostNotification"]) {
		CFNotificationCenterPostNotification(CFNotificationCenterGetDarwinNotifyCenter(), (CFStringRef)[specifier.properties objectForKey:@"PostNotification"], NULL, NULL, YES);
	}
	[super setPreferenceValue:value specifier:specifier];
}

@end
