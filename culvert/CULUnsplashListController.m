#include "CULUnsplashListController.h"

@implementation CULUnsplashListController

- (instancetype)init {
  UIColor *tweakColor = [UIColor colorWithRed:0.00 green:0.00 blue:0.00 alpha:1.0];

  self = [super init];

  if (self) {
    HBAppearanceSettings *appearanceSettings = [[HBAppearanceSettings alloc] init];
    appearanceSettings.tableViewCellTextColor = tweakColor;
    appearanceSettings.tintColor = tweakColor;
		appearanceSettings.invertedNavigationBar = YES;
    self.hb_appearanceSettings = appearanceSettings;
  }

  // [unsplashCollectionID setDelegate:self];

  UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                      action:@selector(save)];

  [self.view addGestureRecognizer:tap];

  return self;
}

- (NSArray *)specifiers {
	if (!_specifiers) {
		_specifiers = [[self loadSpecifiersFromPlistName:@"Unsplash" target:self] retain];
	}

	return _specifiers;
}

-(void)save {
  [self.view endEditing:YES];
}

-(id) readPreferenceValue:(PSSpecifier *)specifier {
		NSDictionary *plistFile = [NSDictionary dictionaryWithContentsOfFile:[NSString stringWithFormat:@"/User/Library/Preferences/%@.plist", [specifier.properties objectForKey:@"defaults"]]];
		if (![plistFile objectForKey:[specifier.properties objectForKey:@"key"]]) {
			return [specifier.properties objectForKey:@"default"];
		}
		return [plistFile objectForKey:[specifier.properties objectForKey:@"key"]];
}

-(void) setPreferenceValue:(id)value specifier:(PSSpecifier *)specifier {
	NSMutableDictionary *plistFile = [[NSMutableDictionary alloc] initWithContentsOfFile:[NSString stringWithFormat:@"/User/Library/Preferences/%@.plist", [specifier.properties objectForKey:@"defaults"]]];
	[plistFile setObject:value forKey:[specifier.properties objectForKey:@"key"]];
	[plistFile writeToFile:[NSString stringWithFormat:@"/User/Library/Preferences/%@.plist", [specifier.properties objectForKey:@"defaults"]] atomically:1];
	if ([specifier.properties objectForKey:@"PostNotification"]) {
		CFNotificationCenterPostNotification(CFNotificationCenterGetDarwinNotifyCenter(), (CFStringRef)[specifier.properties objectForKey:@"PostNotification"], NULL, NULL, YES);
	}
	[super setPreferenceValue:value specifier:specifier];
}

@end
