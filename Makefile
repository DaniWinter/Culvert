include $(THEOS)/makefiles/common.mk

TWEAK_NAME = Culvert
Culvert_FILES = Tweak.xm
Culvert_FRAMEWORKS = UIKit CoreGraphics
Culvert_PRIVATE_FRAMEWORKS = PersistentConnection PhotoLibrary SpringBoardFoundation

include $(THEOS_MAKE_PATH)/tweak.mk

after-install::
	install.exec "killall -9 SpringBoard"
SUBPROJECTS += culvert
include $(THEOS_MAKE_PATH)/aggregate.mk
