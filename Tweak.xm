#include "Tweak.h"

//Crack ass simple DRM check to prevent downloading from pirate repo's
BOOL DRM = [[NSFileManager defaultManager] fileExistsAtPath:@"/var/lib/dpkg/info/nl.d4ni.culvert.list"];

NSString *drmMessage = @"\nYou've downloaded this tweak from a pirate repo, it can have malware injected to it.\n\nThe tweak has been disabled.\n\nPlease download it for free from my repo: https://repo.d4ni.nl/";
NSString *prefBundle = @"/var/mobile/Library/Preferences/nl.d4ni.culvert.plist";
NSString *firstRun = @"/var/mobile/Library/Preferences/nl.d4ni.culvert-firstRun.plist";
NSString *tweakName = @"Culvert";
NSString *bingAPI = @"https://www.bing.com/HPImageArchive.aspx?format=js&idx=0&n=8";
NSString *vellumAPI = @"https://api.d4ni.nl/vellum/";
NSString *unsplashAPI = @"https://source.unsplash.com/";

//Defaults
float blurOpacity = 10.0f;
int wallpaperModePref = 0;
int wallpaperSource = 0;
int perspectiveEnabled = 1;
int blurMode = 0;
BOOL enabled = YES;
BOOL unsplashCollectionEnabled = NO;
BOOL manualUpdate = NO;
BOOL randomImg = NO;
BOOL getNotification = YES;
BOOL saveimg = NO;
BOOL blur = NO;
NSURL *downloadURL;
NSString *imageDescription;
NSString *unsplashCollectionID = @"";
UIImage *image;

%hook SpringBoard

- (void)applicationDidFinishLaunching:(UIApplication *)application {
	%orig;

	manualUpdate = NO;
	if (DRM) {
		if (enabled) {
	    [self _culvert_configureTimer];

	    if([[NSDictionary dictionaryWithContentsOfFile:firstRun] objectForKey:@"firstRun"] == nil) {
	      NSMutableDictionary *preferences = [NSMutableDictionary dictionary];
	      [preferences addEntriesFromDictionary:[NSDictionary dictionaryWithContentsOfFile:firstRun]];
	      [preferences setObject:@"false" forKey:@"firstRun"];
	      [preferences writeToFile:firstRun atomically:YES];

				UIAlertController *alertController1 = [UIAlertController alertControllerWithTitle:tweakName message:@"\nThis is your first time using Culvert, please edit settings as you please and press manual update, enjoy! (:" preferredStyle:UIAlertControllerStyleAlert];
					[alertController1 addAction:[UIAlertAction actionWithTitle:@"OK." style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
						[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=Culvert"]];
					}]];
				[[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertController1 animated:YES completion:NULL];
	    }
	  }
	} else {
		[self triggerDialog:drmMessage];
	}
}

%new - (void)_culvert_configureTimer {
	NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];

	NSDateComponents *dateComponents = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate date]];
	dateComponents.day++;
	dateComponents.hour = 9;
	dateComponents.minute = arc4random_uniform(3); // let's not ddos bing
	dateComponents.second = arc4random_uniform(61);

	PCPersistentTimer *timer = [[[PCPersistentTimer alloc] initWithFireDate:[calendar dateFromComponents:dateComponents] serviceIdentifier:@"nl.d4ni.culvert" target:self selector:@selector(_culvert_updateWallpaper) userInfo:nil] autorelease];
	[timer scheduleInRunLoop:[NSRunLoop mainRunLoop]];
}

%new - (void)_culvert_updateWallpaper {
	int imageNumber = 0;

  if (wallpaperSource == 0) {
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:bingAPI]];
  	NSData *theData = [NSURLConnection sendSynchronousRequest:request
                        returningResponse:nil
                                    error:nil];
    NSDictionary *newJSON = [NSJSONSerialization JSONObjectWithData:theData
                                                          options:0
                                                            error:nil];

    if (randomImg) {
    	imageNumber = arc4random_uniform(7);
    }

    NSString *imageURL = [[newJSON valueForKeyPath:@"images"][imageNumber] objectForKey:@"urlbase"];
  	imageDescription = [[newJSON valueForKeyPath:@"images"][imageNumber] objectForKey:@"copyright"];

    downloadURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://bing.com%@_1080x1920.jpg", imageURL]];
  } else if (wallpaperSource == 1) {
    NSDate *nowDate = [NSDate date];

    if (randomImg) {
      nowDate = [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitDay
                                   value:-arc4random_uniform(8)
                                  toDate:nowDate
                                 options:0];
    }

    NSDateFormatter *dayFormat = [[NSDateFormatter alloc] init]; [dayFormat setDateFormat:@"dd"];
  	NSDateFormatter *monthFormat = [[NSDateFormatter alloc] init]; [monthFormat setDateFormat:@"MM"];
  	NSDateFormatter *yearFormat = [[NSDateFormatter alloc] init]; [yearFormat setDateFormat:@"yyyy"];

  	NSString *dayString = [dayFormat stringFromDate:nowDate];
  	NSString *monthString = [monthFormat stringFromDate:nowDate];
  	NSString *yearString = [yearFormat stringFromDate:nowDate];

    downloadURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@/%@.jpg", vellumAPI, yearString, monthString, dayString]];
  } else if (wallpaperSource == 2) {
		if (unsplashCollectionEnabled && ![unsplashCollectionID isEqualToString:@""]) {
			downloadURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/collection/%@", unsplashAPI, unsplashCollectionID]];
		} else {
			downloadURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/daily", unsplashAPI]];
	    if (randomImg) {
	      downloadURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/random", unsplashAPI]];
	    }
		}
  }

  NSData *data = [NSData dataWithContentsOfURL:downloadURL];
  UIImage *rawImage = [UIImage imageWithData:data];
  //CROP IMAGE
  CGSize screenSize = [SBFWallpaperParallaxSettings minimumWallpaperSizeForCurrentDevice];
  float ratio = screenSize.height/screenSize.width;
  CGRect rect;
  if ((rawImage.size.height/rawImage.size.width)>ratio) {
      // rect = CGRectMake(0.0f, (rawImage.size.height - rawImage.size.width*ratio) * 0.5f, rawImage.size.width, rawImage.size.width*ratio);
      rect = CGRectMake(0.0f, (rawImage.size.height - rawImage.size.width*ratio) * 0.5f, rawImage.size.width, rawImage.size.width*ratio);
  } else {
      // rect = CGRectMake((rawImage.size.width - rawImage.size.height/ratio) * 0.5f, 0.0f, (rawImage.size.height/ratio), rawImage.size.height);
      rect = CGRectMake((rawImage.size.width - rawImage.size.height/ratio) * 0.5f, 0.0f, (rawImage.size.height/ratio), rawImage.size.height);
  }

  CGImageRef imageRef = CGImageCreateWithImageInRect([rawImage CGImage], rect);

  image = [[[UIImage alloc] initWithCGImage:imageRef] autorelease];

	//set wallpaper
  CGImageRelease(imageRef);
  PLStaticWallpaperImageViewController *wallpaperViewController = [[[PLStaticWallpaperImageViewController alloc] initWithUIImage:image] autorelease];
	wallpaperViewController.allowsEditing = YES;
  wallpaperViewController.saveWallpaperData = YES;

	uintptr_t address = (uintptr_t)&wallpaperModePref;
  object_setInstanceVariable(wallpaperViewController, "_wallpaperMode", *(PLWallpaperMode **)address);
	// [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:wallpaperViewController animated:YES completion:NULL]; //Show wallpaper viewcontroller
  [wallpaperViewController _savePhoto];

	if (blur) {
		//set the blurred image with a slight delay otherwise normal wallpaper overrides this.
		double delayInSeconds = 0.5;
		dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
		dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
			CIContext *context = [CIContext contextWithOptions:nil];
			CIImage *inputImage = [CIImage imageWithCGImage:rawImage.CGImage];
			CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
			[filter setValue:inputImage forKey:kCIInputImageKey];
			[filter setValue:[NSNumber numberWithFloat:blurOpacity] forKey:@"inputRadius"];
			CIImage *result = [filter valueForKey:kCIOutputImageKey];
			CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
			image = [UIImage imageWithCGImage:cgImage];
			// if (cgImage) {
				CGImageRelease(cgImage);
			// }

		 PLStaticWallpaperImageViewController *wallpaperViewController = [[[PLStaticWallpaperImageViewController alloc] initWithUIImage:image] autorelease];
		 wallpaperViewController.saveWallpaperData = YES;

		 uintptr_t address = (uintptr_t)&blurMode;
		 object_setInstanceVariable(wallpaperViewController, "_wallpaperMode", *(PLWallpaperMode **)address);
		 [wallpaperViewController _savePhoto];
		});
	}

	//save image to cameraroll
  if (saveimg) {
    UIImageWriteToSavedPhotosAlbum(rawImage, nil, nil, nil);
  }

	if (getNotification && wallpaperSource == 0) {
		UIImage *bundleIcon = [UIImage imageWithContentsOfFile:@"/Library/PreferenceBundles/Culvert.bundle/icon_big.png"];
		[[objc_getClass("JBBulletinManager") sharedInstance] showBulletinWithTitle:@"Culvert"
	                                                           message:imageDescription
	                                                           overrideBundleImage:bundleIcon];
	}

	if (!manualUpdate) {
		[self _culvert_configureTimer];
	}
}

%new - (void) triggerDialog:(NSString *)message {
	UIAlertController *alertController1 = [UIAlertController alertControllerWithTitle:tweakName message:message preferredStyle:UIAlertControllerStyleAlert];
			[alertController1 addAction:[UIAlertAction actionWithTitle:@"OK." style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

			}]];
	[[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertController1 animated:YES completion:NULL];
}
%end

%hook PLStaticWallpaperImageViewController

- (void)providerLegibilitySettingsChanged:(SBSUIWallpaperPreviewViewController *)wallpaperPreviewViewController {
  [wallpaperPreviewViewController setMotionEnabled:perspectiveEnabled];
	%orig(wallpaperPreviewViewController);
}

%end

static void manualWallpaper() {
	if (DRM) {
		manualUpdate = YES;
	  [(SpringBoard *)[UIApplication sharedApplication] _culvert_updateWallpaper];
	} else {
		[(SpringBoard *)[UIApplication sharedApplication] triggerDialog:drmMessage];
	}
}

//Load preferences
static void loadPrefs() {
  NSMutableDictionary *prefs = [[NSMutableDictionary alloc] initWithContentsOfFile:prefBundle];
  if(prefs) {
    enabled = ( [prefs objectForKey:@"enabled"] ? [[prefs objectForKey:@"enabled"] boolValue] : enabled );
    saveimg = ( [prefs objectForKey:@"saveimg"] ? [[prefs objectForKey:@"saveimg"] boolValue] : saveimg );
    randomImg = ( [prefs objectForKey:@"randomImg"] ? [[prefs objectForKey:@"randomImg"] boolValue] : randomImg );
    getNotification = ( [prefs objectForKey:@"getNotification"] ? [[prefs objectForKey:@"getNotification"] boolValue] : getNotification );
    blur = ( [prefs objectForKey:@"blur"] ? [[prefs objectForKey:@"blur"] boolValue] : blur );
    blurOpacity = ( [prefs objectForKey:@"blurOpacity"] ? [[prefs objectForKey:@"blurOpacity"] floatValue] : blurOpacity );
    wallpaperModePref = ( [prefs objectForKey:@"wallpaperMode"] ? [[prefs objectForKey:@"wallpaperMode"] intValue] : wallpaperModePref );
    wallpaperSource = ( [prefs objectForKey:@"wallpaperSource"] ? [[prefs objectForKey:@"wallpaperSource"] intValue] : wallpaperSource );
    perspectiveEnabled = ( [prefs objectForKey:@"perspectiveEnabled"] ? [[prefs objectForKey:@"perspectiveEnabled"] intValue] : perspectiveEnabled );
    blurMode = ( [prefs objectForKey:@"blurMode"] ? [[prefs objectForKey:@"blurMode"] intValue] : blurMode );
    unsplashCollectionEnabled = ( [prefs objectForKey:@"unsplashCollectionEnabled"] ? [[prefs objectForKey:@"unsplashCollectionEnabled"] boolValue] : unsplashCollectionEnabled );
		unsplashCollectionID = [prefs objectForKey:@"unsplashCollectionID"];
  }
  [prefs release];
}

//Initialize event listeners and load preferences.
%ctor {
  CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), NULL, (CFNotificationCallback)manualWallpaper, CFSTR("nl.d4ni.culvert/update"), NULL, CFNotificationSuspensionBehaviorCoalesce);
  CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), NULL, (CFNotificationCallback)loadPrefs, CFSTR("nl.d4ni.culvert/changed"), NULL, CFNotificationSuspensionBehaviorCoalesce);
	loadPrefs();
}
